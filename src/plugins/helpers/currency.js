import {constants} from "../../config";
import Cookies from "js-cookie";

export const CurrencyConverter = (from, to, onNew = null) => {
    const fromCode = constants.currencies[from].code || true;
    const toCode = constants.currencies[to].code || true;
    const key = `${parseInt(String(Math.floor(Date.now() / (1000 * 100))))}_${fromCode}_${toCode}`;
    let value = parseFloat(Cookies.get(key));

    if (value > 0) {
        return Promise.resolve(value)
    }

    if (fromCode === true) {
        return Promise.reject('Not correct "FROM" currency name');
    }

    if (toCode === true) {
        return Promise.reject('Not correct "TO" currency name');
    }

    return fetch(`${constants.currencyInfo.convertUrl}&apiKey=${constants.currencyInfo.apiKey}&q=${fromCode}_${toCode}`)
        .then(res => res.json())
        .then(json => {
            const vals = Object.values(json);

            if (vals.length >= 1) {
                Cookies.set(key, String(vals[0]));

                onNew(vals[0]);

                return vals[0];
            }

            return 0.00;
        });
};
