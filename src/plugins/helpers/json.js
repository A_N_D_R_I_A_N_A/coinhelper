import {constants} from "../../config";

const commonHeaders = Object.freeze({
    'Content-Type': 'application/json',
    'secret-key': constants.jsonStorage.secretKey
});

export const GetStore = () => {
    return fetch(
        `${constants.jsonStorage.baseUrl}${constants.jsonStorage.systemCollection.bins.route}/${constants.jsonStorage.systemCollection.bins.lastVersion.id}/latest`, {
            method: 'GET',
            headers: commonHeaders
        }
    ).then(res => res.json()).then(res => res.store);
};

export const UpdateStore = store => {
    return fetch(
        `${constants.jsonStorage.baseUrl}${constants.jsonStorage.systemCollection.bins.route}/${constants.jsonStorage.systemCollection.bins.lastVersion.id}`,
        {
            method: 'PUT',
            headers: Object.assign({}, commonHeaders, {
                'versioning': 'true'
            }),
            body: JSON.stringify({time: Date.now(), store})
        }
    ).then(res => res.json());
};
