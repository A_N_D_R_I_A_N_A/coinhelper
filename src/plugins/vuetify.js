import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import {constants, globalVariables} from "../config";
import {CurrencyConverter} from "./helpers/currency";

Vue.use(Vuetify);

Vue.use({
    install: v => {
        v.prototype.$g = Vue.observable(globalVariables);
        v.prototype.$c = constants;
        v.prototype.$convertCurrencyValue = CurrencyConverter;
    }
});

export default new Vuetify({
    theme: {
        dark: true,
        options: {
            customProperties: true,
        },
        themes: {
            light: {
                primary: '#ee44aa',
                secondary: '#424242',
                accent: '#82B1FF',
                error: '#FF5252',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FFC107'
            },
        },
    },
    icons: {
        iconfont: 'mdiSvg',
    },
    breakpoint: {
        xs: true,
        sm: true,
        md: false,
        lg: false,
        xl: false,

        xsOnly: true,
        smOnly: true,
        smAndDown: true,
        smAndUp: false,
        mdOnly: false,
        mdAndDown: false,
        mdAndUp: false,
        lgOnly: false,
        lgAndDown: false,
        lgAndUp: false,
        xlOnly: false,

        scrollBarWidth: 25,
    },
});
