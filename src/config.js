export const constants = Object.freeze({
    currencies: {
        BTC: {
            key: 'BTC',
            code: 'BTC',
            title: 'BitCoin',
        },
        USD: {
            key: 'USD',
            code: 'USD',
            title: 'United States Dollar'
        }
    },
    currencyInfo: {
        baseUrl: 'https://free.currconv.com',
        convertUrl: 'https://free.currconv.com/api/v7/convert?compact=ultra',
        apiKey: '66cc3a22c5bf6b3f970c',
    },
    jsonStorage: {
        baseUrl: 'https://api.jsonbin.io',
        secretKey: '$2b$10$0OlQ.R0sVrE0Z5Ws8QmzSeUxRmJ5P6foTiugt.ywF5epPQ.N7Vl7q',
        systemCollection: {
            route: '/c',
            id: "5dc0a9e310cc0c3b8dff65c2",
            data: {
                "name": "Coin Helper Collection",
                "version": "1.0.0"
            },
            bins: {
                route: '/b',

                lastVersion: {
                    id: "5dc0ab4010cc0c3b8dff664a"
                }
            }
        }
    }
});

export const globalVariables = {
    storeInit: false,
    drawer: null
};
