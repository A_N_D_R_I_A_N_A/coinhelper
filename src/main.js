import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import AsyncComputed from 'vue-async-computed'

Vue.use(AsyncComputed);

Vue.config.productionTip = process.env.NODE_ENV !== 'development';

Vue.config.devtools = process.env.NODE_ENV === 'development';

const app = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app');

window.__VUE_DEVTOOLS_GLOBAL_HOOK__.Vue = app.constructor;
