import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import {GetStore, UpdateStore} from "../plugins/helpers/json";

Vue.use(Vuex)

// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = require.context('./modules', true, /\.js$/)

// you do not need `import app from './modules/app'`
// it will auto require all vuex module from modules file
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
    // set './app.js' => 'app'
    const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
    const value = modulesFiles(modulePath)
    modules[moduleName] = value.default;
    return modules
}, {});

Vue.config.devtools = process.env.NODE_ENV === 'development';

const store = new Vuex.Store({
    modules,
    getters,
    mutations: {
        initialiseStore(state, callback) {
            GetStore().then(store => this.replaceState(
                Object.assign(state, store)
            )).then(() => store.subscribe((mutation, state) => UpdateStore(state))).then(() => typeof callback === 'function' ? callback() : null);
        }
    }
});


export default store
